package com.devcamp.devcamp_province.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province.model.Ward;

public interface IWardReponsitory extends JpaRepository<Ward, Long>{
    Ward findById(long id);
}
