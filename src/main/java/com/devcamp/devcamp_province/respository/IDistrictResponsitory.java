package com.devcamp.devcamp_province.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province.model.District;

public interface IDistrictResponsitory extends JpaRepository<District, Long> {
    District findById(long id);   // phuongw thức truy tìm đối tượng qua id 

    
}
