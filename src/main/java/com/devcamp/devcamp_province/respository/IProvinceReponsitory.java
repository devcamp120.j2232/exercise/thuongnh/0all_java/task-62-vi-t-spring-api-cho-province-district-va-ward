package com.devcamp.devcamp_province.respository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamp_province.model.Province;

public interface IProvinceReponsitory extends JpaRepository<Province, Long>{
    // class này là công cụ để giao tiếp với sql
    Province  findById(long id);
}
