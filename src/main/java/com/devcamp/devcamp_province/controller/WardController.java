package com.devcamp.devcamp_province.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.devcamp_province.model.District;
import com.devcamp.devcamp_province.model.Province;
import com.devcamp.devcamp_province.model.Ward;
import com.devcamp.devcamp_province.respository.IDistrictResponsitory;
import com.devcamp.devcamp_province.respository.IProvinceReponsitory;
import com.devcamp.devcamp_province.respository.IWardReponsitory;

@CrossOrigin
@RestController
@RequestMapping("/ward")
public class WardController {


    // 1 load tất cả các ward
    @Autowired
    IWardReponsitory piIWardReponsitory;
     // lấy tất cả danh sách của district 
     @GetMapping("/all")
     public ResponseEntity<List<Ward>> getAllDistrict(){
         try {
             // khai báo arrat list 
             List<Ward> result = new ArrayList<>();
             piIWardReponsitory.findAll().forEach(result::add);
             return   new ResponseEntity<>(result, HttpStatus.OK);
         } catch (Exception e) {
             // TODO: handle exception
             return   new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
         }
     }


    // 2 load danh sách ward theo id của district
    @Autowired
    private IDistrictResponsitory pDistrictResponsitory;

    @GetMapping("/list")
    public ResponseEntity<Set<Ward>> getListWards(
            @RequestParam(value = "districtId", defaultValue = "1") int district) {
        // set giống list nhưng các giá trị của set là duy nhất không có sự trụng lặp
        try {
            // dựa vào id để tìm ra customer . trả về đối tượng vCustomer
            District vDistrict = pDistrictResponsitory.findById(district);
            // nếu vCustomer khác null thì trả về array list các order
            if (vDistrict != null) {
                return new ResponseEntity<>(vDistrict.getWards(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); // Nếu không tìm thấy thông tin về loại xe cho
                                                                         // mã xe được truyền vào, phương thức sẽ trả về
                                                                         // mã trạng thái HTTP
                // là NOT_FOUND và giá trị của đối tượng ResponseEntity là null.
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // báo lỗi máy chủ nội bộ
        }
    }
    // 3 load ward theo id
    @GetMapping("/by_id")
    public ResponseEntity<Object> getDistrictById(@RequestParam(value = "id") long paramId) {
        try {
            Ward _district = piIWardReponsitory.findById(paramId);
            return new ResponseEntity<>(_district, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 4 tạo 1 ward
    @PostMapping("/create")
    public ResponseEntity<Object> createDistrict(@RequestBody District newDistrict) {
        try {
            District _newDistrict = pDistrictResponsitory.save(newDistrict); // lưu data truyền vào sql
            return new ResponseEntity<>(_newDistrict, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



    // 5 update
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> getUpdateDistrict(@PathVariable Long id, @RequestBody Ward paramBody) {
        // tìm đối tượng cần update trong cơ sở dữ liệu
        Optional<Ward> new_ward = piIWardReponsitory.findById(id);
        try {
            if (new_ward.isPresent()) {
                Ward _ward = new_ward.get();
                _ward.setName(paramBody.getName());
                _ward.setPrefix(paramBody.getPrefix());
                _ward.setDistrict(paramBody.getDistrict());
                _ward.setUpdated(new Date());

                return new ResponseEntity<>(piIWardReponsitory.save(_ward), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); // không tìm thấy
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // không tìm thấy
        }
    }

    // 6 xóa
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
        try {
            piIWardReponsitory.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
