package com.devcamp.devcamp_province.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.devcamp_province.model.District;
import com.devcamp.devcamp_province.model.Province;
import com.devcamp.devcamp_province.model.Ward;
import com.devcamp.devcamp_province.respository.IDistrictResponsitory;
import com.devcamp.devcamp_province.respository.IProvinceReponsitory;

@CrossOrigin
@RestController

@RequestMapping("/district")
public class DistrictController {

    @Autowired
    private IProvinceReponsitory provinceReponsitory;

    // Viết API CRUD District(Quận) của 1 Thành phố và test trên postman
    // lấy danh sách theo id của province
    @GetMapping("/list")
    public ResponseEntity<Set<District>> getListDistrict(
            @RequestParam(value = "province", defaultValue = "1") long provinceId) {
        // set giống list nhưng các giá trị của set là duy nhất không có sự trụng lặp
        try {
            // dựa vào id để tìm ra customer . trả về đối tượng vCustomer
            Province vProvince = provinceReponsitory.findById(provinceId);
            // nếu vCustomer khác null thì trả về array list các order
            if (vProvince != null) {
                System.out.println(vProvince);
                return new ResponseEntity<>(vProvince.getDistricts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); // Nếu không tìm thấy thông tin về loại xe cho
                                                                         // mã xe được truyền vào, phương thức sẽ trả về
                                                                         // mã trạng thái HTTP
                // là NOT_FOUND và giá trị của đối tượng ResponseEntity là null.
            }

        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);

            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // báo lỗi máy chủ nội bộ
        }
    }

    @Autowired
    private IDistrictResponsitory pDistrictResponsitory;

    // lấy tất cả danh sách của district
    @GetMapping("/all")
    public ResponseEntity<List<District>> getAllDistrict() {
        try {
            // khai báo arrat list
            List<District> result = new ArrayList<>();
            pDistrictResponsitory.findAll().forEach(result::add);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tạo mới 1 quận huyện
    @PostMapping("/create")
    public ResponseEntity<Object> createDistrict(@RequestBody District newDistrict) {
        try {
            District _newDistrict = pDistrictResponsitory.save(newDistrict); // lưu data truyền vào sql
            return new ResponseEntity<>(_newDistrict, HttpStatus.CREATED);

        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // đọc quận huyện theo id
    @GetMapping("/by_id")
    public ResponseEntity<Object> getDistrictById(@RequestParam(value = "id") long paramId) {
        try {
            District _district = pDistrictResponsitory.findById(paramId);
            return new ResponseEntity<>(_district, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update quaanh huyện

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> getUpdateDistrict(@PathVariable Long id, @RequestBody District paramBody) {
        // tìm đối tượng cần update trong cơ sở dữ liệu
        Optional<District> district = pDistrictResponsitory.findById(id);
        try {
            if (district.isPresent()) {
                District _district = district.get();
                _district.setName(paramBody.getName());
                _district.setPrefix(paramBody.getPrefix());
                _district.setProvince(paramBody.getProvince());
                _district.setWards(paramBody.getWards());

                return new ResponseEntity<>(pDistrictResponsitory.save(_district), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND); // không tìm thấy
            }
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR); // không tìm thấy
        }
    }

    // xóa quận huyeenh theo id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
        try {
            pDistrictResponsitory.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
