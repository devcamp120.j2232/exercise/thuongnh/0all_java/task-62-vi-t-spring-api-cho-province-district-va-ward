package com.devcamp.devcamp_province.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.devcamp_province.model.District;
import com.devcamp.devcamp_province.model.Province;
import com.devcamp.devcamp_province.model.Ward;
import com.devcamp.devcamp_province.respository.IDistrictResponsitory;
import com.devcamp.devcamp_province.respository.IProvinceReponsitory;


@CrossOrigin
@RestController

public class ProvinceController {

    @Autowired
    private IProvinceReponsitory provinceReponsitory;
    // Viết API cho phép CRUD Province(Thành phố) và test trên postman

    // Tạo Spring boot API cho Province để load ra toàn bộ các Thành phố (R)
    @GetMapping("/provinces")
    public ResponseEntity<List<Province>> getProvinces() {
        try {
            List<Province> result = new ArrayList<Province>();
            provinceReponsitory.findAll().forEach(result::add);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // (C) Viết API cho phép tạo mới tỉnh (thành phố )

    @PostMapping("/provinces/create")
    public ResponseEntity<Object> createUser(@RequestBody Province provinceFormClient) {

        try {
            // khởi tạo 1 đối tượng để lưu kết người dùng cần tọa
            Province _province = new Province(provinceFormClient.getCode(), provinceFormClient.getName(),
                    provinceFormClient.getDistricts());
            Date _now = new Date(); // lấy ngày tạo từ hệ thông
            _province.setCreated(_now);
            _province.setUpdated(null);
            provinceReponsitory.save(_province);
            return new ResponseEntity<Object>(_province, HttpStatus.OK);

            // "error": "Unsupported Media Type",

        } catch (Exception e) {
            // TODO: handle exception
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to create specified user" + e.getCause().getCause().getMessage());

        }
    }
    //  // (U) Viết API cho phép tạo mới tỉnh (thành phố )  

    @PutMapping("/provinces/update/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody Province pProvince) {
        try {
            Optional<Province> countryData = provinceReponsitory.findById(id);
		if (countryData.isPresent()) {
            System.out.println("Province");
            Date _now = new Date(); // lấy ngày tạo từ hệ thông
           
			Province newCountry = countryData.get();
            newCountry.setUpdated(_now);
			newCountry.setName(pProvince.getName());
			newCountry.setCode(pProvince.getCode());
			newCountry.setDistricts(pProvince.getDistricts());
			Province savedCountry = provinceReponsitory.save(newCountry);    //  đối tượng mới sau update 
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
            System.out.println("countryData41414");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
            
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
		
	}
     //  // (D) Viết API cho phép xóa  tỉnh (thành phố )   theo id 

	@DeleteMapping("/provinces/delete/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			provinceReponsitory.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}



    @GetMapping("/provinces/page")
    public ResponseEntity<List<Province>> getFiveVoucher(
            @RequestParam(value = "page", defaultValue = "1") String page,
            @RequestParam(value = "size", defaultValue = "5") String size) {
        try {
            Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
            List<Province> list = new ArrayList<Province>();
            provinceReponsitory.findAll(pageWithFiveElements).forEach(list::add);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            return null;
        }
    }


  

}
