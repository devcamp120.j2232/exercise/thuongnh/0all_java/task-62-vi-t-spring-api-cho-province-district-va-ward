package com.devcamp.devcamp_province;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampProvinceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampProvinceApplication.class, args);
	}

}
