package com.devcamp.devcamp_province.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "district")
public class District {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "prefix")
    private String prefix;

   

    // thêm ngày tạo ngày cập nhập
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate // ngày tạo
    @Column(name = "created", nullable = true, updatable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate // tự sinh ra ngay updated
    @Column(name = "updated", nullable = true, updatable = true)
    private Date updated;

    @ManyToOne // nhiều 1
    @JsonIgnore // xử lý đê quy
    @JoinColumn(name = "province_id")  // dòng này để liên kết với lớp province 
    private Province province;

    // 1 nhiều
    @OneToMany(targetEntity = Ward.class, cascade = CascadeType.ALL) // để xác định quan hệ giữa hai đối tượng trong
    @JoinColumn(name = "district_id") // để giải quyết vấn đề vòng lặp vô hạn
    private Set<Ward> wards;

    public District() {
    }

    public District(long id, String name, String prefix, Province province, Set<Ward> wards) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.wards = wards;
    }

    public District(long id, String name, String prefix, Province province, Date created, Date updated,
            Set<Ward> wards) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
        this.created = created;
        this.updated = updated;
        this.wards = wards;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public Set<Ward> getWards() {
        return wards;
    }

    public void setWards(Set<Ward> wards) {
        this.wards = wards;
    }

    // id: int,
    // name: String,
    // prefix: String,
    // province: Province,
    // wards: Set<Ward>

}
