package com.devcamp.devcamp_province.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "province")
public class Province {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    // thêm ngày tạo ngày cập nhập
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate // ngày tạo
    @Column(name = "created", nullable = true, updatable = false)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate // tự sinh ra ngay updated
    @Column(name = "updated", nullable = true, updatable = true)
    private Date updated;

    // 1 nhiều
    @OneToMany(targetEntity = District.class , cascade = CascadeType.ALL) // để xác định quan hệ giữa hai đối tượng trong
    @JoinColumn(name = "province_id")                                                            // Hibernate
   // @JsonIgnore // để giải quyết vấn đề vòng lặp vô hạn
    private Set<District> districts;

    public Province() {
    }

    public Province(String code, String name, Set<District> districts) {
        this.code = code;
        this.name = name;
        this.districts = districts;
    }

    public Province(long id, String code, String name, Date created, Date updated, Set<District> districts) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.created = created;
        this.updated = updated;
        this.districts = districts;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

}
